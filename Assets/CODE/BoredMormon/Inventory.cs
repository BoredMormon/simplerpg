﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BoredMormon {
    public class Inventory : MonoBehaviour {

        GameObject itemInRange;
        GameObject currentItem;

        [SerializeField]
        Image displayImage;
        [SerializeField]
        Text displayText;
        [SerializeField]
        Transform dropPosition;

        // Update is called once per frame
        void Update() {
            if (Input.GetButtonDown("Fire1"))
            {
                if (currentItem)
                {
                    currentItem.transform.position = dropPosition.position;
                    currentItem.SetActive(true);
                    NewItemInRange(currentItem.GetComponent<Item>());
                    currentItem = null;
                }
                else if (itemInRange)
                {
                    itemInRange.SetActive(false);
                    currentItem = itemInRange;
                    ItemLeftRange();
                }
            }
        }

        void NewItemInRange (Item item)
        {
            itemInRange = item.gameObject;
            if (displayText)
            {
                displayText.gameObject.SetActive(true);
                displayText.text = "Press Fire1 to pick up " + itemInRange.name;
            }
        }

        void ItemLeftRange()
        {
            itemInRange = null;
            if (displayText)
            {
                displayText.gameObject.SetActive(false);
                displayText.text = "";
            }
        }

        void OnTriggerEnter(Collider other)
        {
            Item item = other.GetComponent<Item>();
            if (item)
            {
                NewItemInRange(item);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject == itemInRange)
            {
                ItemLeftRange();
            }
        }
    }
}