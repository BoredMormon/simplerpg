﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionCanvas : MonoBehaviour {

	[SerializeField]
	private Animator iconPrefab;

	private Dictionary<Transform, Animator> icons;
	private Stack<Animator> pool;

	private static InteractionCanvas instance;
	public static InteractionCanvas Instance {
		get {
			return instance;
		}
	}

	void Awake() {
		instance = this;
		icons = new Dictionary<Transform, Animator>();
		pool = new Stack<Animator>();
	}

	void Update() {
		foreach(Transform anchor in icons.Keys) {
			Animator iconAnimator = icons[anchor];
			iconAnimator.transform.position = Camera.main.WorldToScreenPoint(anchor.position);
		}
	}

	public void AddIcon(Transform anchor, KeyCode keyCode) {
		Animator animator;

		if(icons.ContainsKey(anchor)) {
			animator = icons[anchor];
		} else {
			animator = GetNewIcon();
			icons.Add(anchor, animator);
		}

		animator.transform.Find("Backing/Text").GetComponent<Text>().text = KeyCodeToString(keyCode);
		animator.SetBool("IsActive", true);
	}

	public void RemoveIcon(Transform anchor) {
		if(icons.ContainsKey(anchor)) {
			Animator animator = icons[anchor];
			StartCoroutine(AddToPoolDelayed(anchor, animator));
			animator.SetBool("IsActive", false);
		}
	}

	public void ActivateIcon(Transform anchor) {
		if(icons.ContainsKey(anchor)) {
			icons[anchor].SetTrigger("Activate");
			RemoveIcon(anchor);
		}
	}

	IEnumerator AddToPoolDelayed(Transform anchor, Animator animator, float delay = 5f) {
		yield return new WaitForSeconds(delay);
		if(animator.GetBool("IsActive") == false) {
			icons.Remove(anchor);
			pool.Push(animator);
		}
	}

	Animator GetNewIcon() {
		Animator animator;

		if(pool.Count > 0) {
			animator = pool.Pop();
		} else {
			animator = Instantiate<Animator>(iconPrefab);
			animator.transform.SetParent(transform, false);
		}

		return animator;
	}

	string KeyCodeToString(KeyCode keyCode) {
		char letter = (char)keyCode;
		return letter.ToString().ToUpper();
	}

}
