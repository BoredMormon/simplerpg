﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace DanielQuick {

	/// <summary>
	/// Display the contents of signs in the UI
	/// Click on the content when open to exit
	/// </summary>
	public class SignCanvas : MonoBehaviour {

		[SerializeField]
		private Text txtMessage;

		private static SignCanvas instance;
		public static SignCanvas Instance {
			get {
				return instance;
			}
		}

		private Animator animator;

		void Awake() {
			instance = this;
			animator = GetComponent<Animator>();
		}

		public void ShowMessage(string message) {
			animator.SetBool("IsShowing", true);
			txtMessage.text = message;
		}

		public void HideMessage() {
			animator.SetBool("IsShowing", false);
		}

		public void HideMessage(string message) {
			if(txtMessage.text.Equals(message)) {
				animator.SetBool("IsShowing", false);
			}
		}

	}

}