﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCommunityProject;

namespace DanielQuick {

	public class UFO : MonoBehaviour, IInteract {

		[SerializeField]
		InputInteractTrigger trigger;

		[SerializeField]
		private Transform playerParent;
		[SerializeField]
		private Transform exitPosition;

		private UFOLocomotion locomotion;

		private UFOStatus status;

		public UFOStatus Status {
			get {
				return status;
			}
		}

		void Awake() {
			locomotion = GetComponent<UFOLocomotion>();
		}

		public void Interact() {
			status = UFOStatus.Lifting;
			locomotion.LiftOff(OnFlying);
			Player.Instance.GetComponent<Rigidbody>().isKinematic = true;
			Player.Instance.transform.SetParent(playerParent);
			Player.Instance.transform.localPosition = Vector3.zero;
			Player.Instance.GetComponent<PlayerControl>().enabled = false;
		}

		void OnFlying() {
			status = UFOStatus.Flying;
		}

		void OnGrounded() {
			status = UFOStatus.Grounded;
			Player.Instance.GetComponent<Rigidbody>().isKinematic = false;
			Player.Instance.transform.SetParent(null);
			Player.Instance.transform.position = exitPosition.position;
			Player.Instance.GetComponent<PlayerControl>().enabled = true;
			trigger.Reset();
		}

		public void EndInteract() {
			
		}

		void ExitUFO() {
			status = UFOStatus.Landing;
			locomotion.Land(OnGrounded);
		}

		void Update() {
			if(locomotion.enabled && status == UFOStatus.Flying) {
				if(Input.GetKeyDown(KeyCode.E)) {
					ExitUFO();
				}
			}
		}

	}

	public enum UFOStatus {
		Grounded,
		Lifting,
		Flying,
		Landing
	}

}