﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DanielQuick {

	public class UFOLocomotion : MonoBehaviour {

		[SerializeField]
		private float speed = 10f;
		[SerializeField]
		private float flightHeight = 10f;
		[SerializeField]
		private float raiseSpeed = 20f;

		private UFO ufo;

		private new Rigidbody rigidbody;

		void Awake() {
			rigidbody = GetComponent<Rigidbody>();
			ufo = GetComponent<UFO>();
		}

		public void LiftOff(System.Action callback) {
			StartCoroutine(LiftOffCoroutine(callback));
		}

		IEnumerator LiftOffCoroutine(System.Action callback) {
			float startHeight = transform.position.y;
			float targetHeight = transform.position.y + flightHeight;
			while(transform.position.y < targetHeight) {
				Vector3 position = transform.position;
				position.y = Mathf.Clamp(transform.position.y + raiseSpeed * Time.deltaTime, startHeight, targetHeight);
				transform.position = position;
				yield return null;
			}

			callback.Invoke();
		}

		public void Land(System.Action callback) {
			StartCoroutine(LandCoroutine(callback));
		}

		IEnumerator LandCoroutine(System.Action callback) {
			float startHeight = transform.position.y;
			float targetHeight;
			RaycastHit hit;
			if(Physics.Raycast(transform.position, Vector3.down, out hit)) {
				targetHeight = hit.point.y;
			} else {
				targetHeight = 0f;
			}

			while(transform.position.y > targetHeight) {
				Vector3 position = transform.position;
				position.y = Mathf.Clamp(transform.position.y - raiseSpeed * Time.deltaTime, targetHeight, startHeight);
				transform.position = position;
				yield return null;
			}

			callback.Invoke();
		}

		void Update() {
			if(ufo.Status == UFOStatus.Flying) {
				Vector3 direction;
				direction.y = 0;
				direction.x = Input.GetAxis("Horizontal");
				direction.z = Input.GetAxis("Vertical");
				direction.Normalize();

				rigidbody.MovePosition(transform.position + direction * speed * Time.deltaTime);
			}
		}

	}

}