﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UnityCommunityProject
{
    public class MissileMover : MonoBehaviour
    {

        [Header("REFERENCES")]
        public GameObject go;

        [Header("VARIABLES")]
        public Vector3 Direction;
        public float Speed;
        public float ExpireTime;
        public float MaxDistance;
        float StartTime;
        Vector3 StartPosition;

        [Header("FLAGS")]
        public bool tf;

        void Start()
        {
            StartTime = Time.time;
            StartPosition = transform.position;
        }


        void Update()
        {
            transform.position += Direction * Speed * Time.deltaTime;
            if (Time.time - StartTime > ExpireTime)
            {
                Destroy(this.gameObject);
            }
            if (Vector3.Distance(transform.position, StartPosition) > MaxDistance)
            {
                Destroy(this.gameObject);
            }
        }
    }
}